# Les Patelles déménagent... ou pas!



## Introduction et généralités

Bienvenue sur le notre repo GitLab vous permettant d'accéder à l'ensemble des données nous ayant permis de mener le projet patelles au cours du dernier semestre de Licence 3.

## Utilisation

Pour consulter l'étude dans son intégralité, vous pouvez téléharger le dossier [Sujet1-Patelles](https://gitlab.com/julescrch/les-patelles-demenagent-ou-pas-l3s6/-/blob/main/Sujet1-Patelles). Il vous suffit ensuite de lire le fichier "patella.pdf" ou bien de lancer le fichier Rstudio "patella.Rmd" si vous souhaitez consulter l'intégralité des données.

## Auteurs et remerciements
Ce dossier a été rédigé par Matthias Cornevin, Titouan Cozic, Jules Créach et Gaël Girard. Nos remerciements s'adressent à Frédéric JEAN, responsable de l'UE et professeur en charge de la supervision de notre étude.


## License
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />Ce(tte) œuvre est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.

